'use strict'

const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
//Devuelve un hash con salt incluido;

function encriptaPassword( password)
{
    return bcrypt.hash(password, 10);
}

//Devuelve true o false
function comparaPassword (password, hash)
{
    return bcrypt.compare(password, hash);
}

module.exports = {
    encriptaPassword,
    comparaPassword
};


