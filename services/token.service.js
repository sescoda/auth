'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');
const mongoose = require('mongoose');
const SECRET = require('../config').SECRET;
const EXP_TIME = require('../config').TOKEN_EXP_TIME;

// Crear Token JWT
function crearToken (user){
    const payload = {
        sub:user._id,
        iat: moment().unix(),
        expira: moment().add(EXP_TIME, 'minutes').unix()
    };
    return jwt.encode(payload, SECRET);
}

function decodificarToken (token){
    return new Promise( (resolve, reject) => {
        try{
            const payload = jwt.decode(token, SECRET, true );
            if(payload.expira <= moment().unix()) {
                reject( {
                    status: 401,
                    message: 'El token ha expirado'
                });
            }
            resolve( payload.sub);

        } catch(err){
            reject( {
                status: 500,
                message: 'El token no es valido',
            });
        }
    });
}

module.exports = {
    crearToken,
    decodificarToken
};